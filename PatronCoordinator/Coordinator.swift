//
//  Coordinator.swift
//  PatronCoordinator
//
//  Created by Daniel on 19/02/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator {
    var childCoordinators:[Coordinator] {get set}
    var navigationController: UINavigationController {get set}
    
    func start()
}
